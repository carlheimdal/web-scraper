const puppeteer = require('puppeteer');
const chalk = require('chalk');
const fs = require('fs');

console.log('hello');
(async() =>{
    try {
        console.log('trying');
        // Open headless browser
        var browser = await puppeteer.launch({headless: true});
        // Open new page
        var page = await browser.newPage();
        // Enter url in page
        await page.goto('https://nrk.no');
        // console.log('found nrk');
        await page.waitForSelector('[class="kur-room__content"] > h2 > span');
        // console.log('Found selector');
        let news = await page.evaluate(() =>{
           let newsTitles = document.querySelectorAll('[class="kur-room__content"] > h2 > span');

           let newsTitleArray = [];
           for(let i = 0; i < newsTitles.length; i++){
               newsTitleArray[i] = {
                   title: newsTitles[i].innerText.trim()
               };
           }
            return newsTitleArray;
        });
        // console.log(news);
        await browser.close();
        fs.writeFile('nrkNewsTitles.json', JSON.stringify(news), function(err){
            if(err) throw err;
            console.log('Saved to JSON.');
        });
        // console.log('Browser closed');
    } catch (err) {
        console.log(err);
        await browser.close();
        // console.log('Browser closed.');
    }
})();
