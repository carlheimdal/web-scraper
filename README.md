# Simple webscraper

## Description
Scrapes for all news titles at https://nrk.no
and saves them in a JSON file.

## Usage
>npm start

## Suggestions
Should also add url to each JSON entry.